package cron

import (
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/robfig/cron/v3"
)

var _ cron.Logger = (*logger)(nil)

const (
	logKey = "cron"
)

type logger struct {
}

// DefaultLogger is used by Cron if none is specified.
var DefaultLogger = NewLogger()

func NewLogger() cron.Logger {
	return &logger{}
}

func (l *logger) Info(msg string, keysAndValues ...interface{}) {
	log.Info(msg, fmt.Sprint(keysAndValues...))
}
func (l *logger) Error(err error, msg string, keysAndValues ...interface{}) {
	log.Error(err, msg, fmt.Sprint(keysAndValues...))
}
