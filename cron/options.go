package cron

import "github.com/robfig/cron/v3"

func WithCronOption(opts ...cron.Option) ServerOption {
	return func(o *Server) {
		o.cronOptions = append(o.cronOptions, opts...)
	}
}

func WithChain(wrappers ...cron.JobWrapper) ServerOption {
	return func(o *Server) {
		o.cronOptions = append(o.cronOptions, cron.WithChain(wrappers...))
	}
}
