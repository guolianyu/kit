package jwtx

import (
	"github.com/golang-jwt/jwt/v4"
	"time"
)

/*
	选项模式
*/

type options struct {
	secretKey     string
	signingMethod jwt.SigningMethod
	issuer        string
	audience      string
	expired       time.Duration
}

type JwtTokenOptions func(*options)

// WithSecretKey 设置jwt密钥
func WithSecretKey(secretKey string) JwtTokenOptions {
	return func(o *options) {
		o.secretKey = secretKey
	}
}

// WithSigningMethod 设置签名方法
func WithSigningMethod(signingMethod jwt.SigningMethod) JwtTokenOptions {
	return func(o *options) {
		o.signingMethod = signingMethod
	}
}

// WithIssuer 设置发布者
func WithIssuer(issuer string) JwtTokenOptions {
	return func(o *options) {
		o.issuer = issuer
	}
}

// WithAudience 设置 audience
func WithAudience(audience string) JwtTokenOptions {
	return func(o *options) {
		o.audience = audience
	}
}

// WithExpired 设置过期时间
func WithExpired(expired time.Duration) JwtTokenOptions {
	return func(o *options) {
		o.expired = expired
	}
}

// WithSecretKeyCallback 通过回调设置jwt令牌密钥，当需要密钥时，将调用回调
func WithSecretKeyCallback(callback func() string) JwtTokenOptions {
	return func(o *options) {
		o.secretKey = callback()
	}
}
