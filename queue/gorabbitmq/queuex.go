package gorabbitmq

import (
	"gitee.com/guolianyu/kit/skit/third_party/config"
	"github.com/wagslane/go-rabbitmq"
)

/*
	此mq采用的包地址是：github.com/wagslane/go-rabbitmq
	支持：
	1、自动连接
	2、通过处理函数的多线程消费者
	3、合理的默认值
	4、流量控制处理
	5、TCP块处理

*/

// NewRabbitMQConn 需要注入的函数 wire,创建mq连接
func NewRabbitMQConn(cfg *config.Rabbitmq) (*rabbitmq.Conn, error) {
	return rabbitmq.NewConn(
		cfg.Addrs[0],
		rabbitmq.WithConnectionOptionsLogging,
	)
}
