package gorabbitmq

import "github.com/wagslane/go-rabbitmq"

type ServerOption func(o *ConsumerServer)

// WithConsumerConcurrency 设置消费者消费线程数量
func WithConsumerConcurrency(cc int64) ServerOption {
	return func(o *ConsumerServer) {
		o.globalConsumerOptions = append(o.globalConsumerOptions, rabbitmq.WithConsumerOptionsConcurrency(int(cc)))
	}
}

// WithConsumerQosPrefetch prefetch允许为每个消费者指定最大的unacked消息数目
func WithConsumerQosPrefetch(qosPrefetch int64) ServerOption {
	return func(o *ConsumerServer) {
		if qosPrefetch == 0 {
			qosPrefetch = 300
		}
		o.globalConsumerOptions = append(o.globalConsumerOptions, rabbitmq.WithConsumerOptionsQOSPrefetch(int(qosPrefetch)))
	}
}
