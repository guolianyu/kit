package validate

import (
	"github.com/go-playground/locales"
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/pt"
	"github.com/go-playground/locales/pt_BR"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	enTranslations "github.com/go-playground/validator/v10/translations/en"
	ptTranslations "github.com/go-playground/validator/v10/translations/pt"
	ptBRTranslations "github.com/go-playground/validator/v10/translations/pt_BR"
	zhTranslations "github.com/go-playground/validator/v10/translations/zh"
)

type LocalesFunc func() locales.Translator

var localesNew = map[string]LocalesFunc{
	"zh":    zh.New,
	"en":    en.New,
	"pt":    pt.New,
	"pt_BR": pt_BR.New,
}

type RegisterTranslationsFunc func(v *validator.Validate, trans ut.Translator) (err error)

var defaultRegisterTranslationsFunc = map[string]RegisterTranslationsFunc{
	"zh":    zhTranslations.RegisterDefaultTranslations,
	"en":    enTranslations.RegisterDefaultTranslations,
	"pt":    ptTranslations.RegisterDefaultTranslations,
	"pt_BR": ptBRTranslations.RegisterDefaultTranslations,
}

func NewLocales(lang ...string) []locales.Translator {
	var translators []locales.Translator
	for _, l := range lang {
		if fn, ok := localesNew[l]; ok {
			translators = append(translators, fn())
		}
	}
	return translators
}
