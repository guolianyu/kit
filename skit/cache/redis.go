package cache

import (
	"context"
	"encoding/json"
	"errors"
	"gitee.com/guolianyu/kit/skit/third_party/config"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
	"time"
)

/*
	redis
	添加选项模式
	采集监控
*/

var ErrNotFound = errors.New("not found")

type Option func(*opt) error

func WithTracing(options ...redisotel.TracingOption) Option {
	return func(o *opt) (err error) {
		o.openTracing = true
		o.tracingOption = append(o.tracingOption, options...)
		return
	}
}

func WithMetrics(options ...redisotel.MetricsOption) Option {
	return func(o *opt) (err error) {
		o.openMetrics = true
		// 开启 tracing instrumentation.
		o.metricsOption = append(o.metricsOption, options...)
		return
	}
}
func Config(c *config.Redis) Option {
	return func(o *opt) error {
		o.cfg = c
		return nil
	}
}

type opt struct {
	cfg           *config.Redis
	openTracing   bool
	tracingOption []redisotel.TracingOption

	openMetrics   bool
	metricsOption []redisotel.MetricsOption
}

func NewRedisClient(cfg *config.Redis, options ...Option) (redis.UniversalClient, func(), error) {
	o := &opt{
		cfg:           cfg,
		openTracing:   true,
		tracingOption: make([]redisotel.TracingOption, 0),
		openMetrics:   true,
		metricsOption: make([]redisotel.MetricsOption, 0),
	}

	for _, option := range options {
		err := option(o)
		if err != nil {
			return nil, nil, err
		}
	}

	c := &redis.UniversalOptions{
		Addrs:        cfg.GetAddrs(),
		DB:           int(cfg.GetDb()),
		Username:     cfg.GetUserName(),
		Password:     cfg.GetPassword(),
		DialTimeout:  cfg.GetDialTimeout().AsDuration(),
		ReadTimeout:  cfg.GetReadTimeout().AsDuration(),
		WriteTimeout: cfg.GetWriteTimeout().AsDuration(),
		PoolSize:     int(cfg.GetPoolSize()),
		PoolTimeout:  cfg.GetPoolTimeout().AsDuration(),
	}

	rdb := redis.NewUniversalClient(c)

	if o.openTracing {
		err := redisotel.InstrumentTracing(rdb, o.tracingOption...)
		if err != nil {
			return nil, nil, err
		}
	}

	if o.openMetrics {
		err := redisotel.InstrumentMetrics(rdb, o.metricsOption...)
		if err != nil {
			return nil, nil, err
		}
		rdb.AddHook(RedisMetricsHook{})
	}
	return rdb, func() {
		_ = rdb.Close()
	}, nil
}

func RedisSetWithStructsJSON(cli redis.UniversalClient, ctx context.Context, key string, value interface{}, expiration time.Duration) (err error) {
	data, err := json.Marshal(value)
	if err != nil {
		return
	}
	err = cli.Set(ctx, key, string(data), expiration).Err()
	if err != nil {
		return
	}
	return
}

func RedisGetStructsWithJSON(cli redis.UniversalClient, ctx context.Context, key string, out interface{}) (err error) {

	data, err := cli.Get(ctx, key).Result()
	if !(err == nil || err == redis.Nil || err == context.Canceled) {
		return
	}

	if len(data) == 0 {
		return ErrNotFound
	}

	err = json.Unmarshal([]byte(data), &out)
	if err == nil {
		return nil
	}

	if err = cli.Del(ctx, key).Err(); err != nil {
		log.Errorf("delete invalid cache,err:%v", err)
	}

	return ErrNotFound

}
