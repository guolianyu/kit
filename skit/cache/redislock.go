package cache

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"time"
)

/*
	分布式redis锁
*/

var (
	ErrNotGetLock  = errors.New("not get lock")
	ErrClientNil   = errors.New("redis client nil")
	ErrLockNotHeld = errors.New("not held lock")
	ErrLockTimeout = errors.New("lock timeout")
)

type RedisLockClient struct {
	r redis.UniversalClient
}

func NewRedisLock(r redis.UniversalClient) *RedisLockClient {
	return &RedisLockClient{r: r}
}

type LockAttr struct {
	Key string
	Val string
}

// RetryLock 可重试的分布式锁，指定重试的超时时间，程序内部是100ms重试一次，retryExpires超时时间需要大于100ms
func (c *RedisLockClient) RetryLock(key string, ttl, retryExpires time.Duration) (res *LockAttr, err error) {
	res, err = c.Lock(key, ttl)
	if err == nil {
		return
	}
	if err != ErrNotGetLock {
		return
	}
	//锁被占用，则定时重试
	ctx, cancelFunc := context.WithTimeout(context.Background(), retryExpires)
	defer cancelFunc()

	ticker := time.NewTicker(time.Millisecond * 100)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			res, err = c.Lock(key, ttl)
			if err == nil {
				return
			}
			if err != ErrNotGetLock {
				return
			}
		case <-ctx.Done():
			err = ErrLockTimeout
			return
		}
	}
}

// Lock 获取锁，获取不到立马返回
func (c *RedisLockClient) Lock(key string, ttl time.Duration) (res *LockAttr, err error) {
	if c.r == nil {
		err = ErrClientNil
		return
	}
	val := uuid.NewString()
	success, err := c.r.SetNX(context.Background(), key, val, ttl).Result()
	if err != nil {
		return
	}
	if !success {
		err = ErrNotGetLock
		return
	}
	res = &LockAttr{
		Key: key,
		Val: val,
	}
	return
}

// Release 释放锁
func (c *RedisLockClient) Release(key, val string) (err error) {
	if c.r == nil {
		err = ErrClientNil
		return
	}
	script := "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end"
	rst, err := c.r.Eval(context.Background(), script, []string{key}, val).Int()
	if err != nil {
		return
	}
	if rst != 1 {
		err = ErrLockNotHeld
		return
	}
	return
}
