package cache

import (
	"context"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/redis/go-redis/v9"
	"net"
	"time"
)

var (
	_redisMetricSeconds = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "redis",
		Subsystem: "requests",
		Name:      "duration_sec",
		Help:      "redis requests duration(sec).",
		Buckets:   []float64{0.005, 0.01, 0.025, 0.05, 0.1, 0.250, 0.5, 1},
	}, []string{"name", "service_id", "service_instance", "cmd", "status"})
	_redisMetricRequests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "redis",
		Subsystem: "requests",
		Name:      "cmd_total",
		Help:      "The total number of processed requests",
	}, []string{"name", "service_id", "service_instance", "cmd", "status"})
)

type RedisMetricsHook struct {
	Name            string
	ServiceID       string
	ServiceInstance string
}

func (h RedisMetricsHook) DialHook(hook redis.DialHook) redis.DialHook {
	return func(ctx context.Context, network, addr string) (net.Conn, error) {
		conn, err := hook(ctx, network, addr)
		return conn, err
	}
}

func (h RedisMetricsHook) ProcessHook(hook redis.ProcessHook) redis.ProcessHook {
	return func(ctx context.Context, cmd redis.Cmder) error {
		startTime := time.Now()

		err := hook(ctx, cmd)
		status := "success"
		if cmd.Err() != nil {
			status = "fail"
		}
		_redisMetricRequests.WithLabelValues(h.Name, h.ServiceID, h.ServiceInstance, cmd.Name(), status).Inc()
		_redisMetricSeconds.WithLabelValues(h.Name, h.ServiceID, h.ServiceInstance, cmd.Name(), status).Observe(time.Since(startTime).Seconds())
		return err
	}
}

func (h RedisMetricsHook) ProcessPipelineHook(hook redis.ProcessPipelineHook) redis.ProcessPipelineHook {
	return func(ctx context.Context, cmds []redis.Cmder) error {
		err := hook(ctx, cmds)
		for _, cmd := range cmds {
			status := "success"
			if cmd.Err() != nil {
				status = "fail"
			}
			_redisMetricRequests.WithLabelValues(h.Name, h.ServiceID, h.ServiceInstance, cmd.Name(), status).Inc()
		}
		return err
	}
}

func init() {
	prometheus.MustRegister(
		_redisMetricRequests,
		_redisMetricSeconds,
	)
}
