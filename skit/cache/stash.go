package cache

import (
	"context"
	"errors"
	"gitee.com/guolianyu/kit/str"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/redis/go-redis/v9"
	"reflect"
	"time"
)

/*
	创建hash缓存对象，修改或者新增字段的值；
	字段的值可递增或覆盖
*/

// RecordNotExistsHandler 匿名函数，当缓存的数据不存在的时候执行此函数
type RecordNotExistsHandler func() (interface{}, error)

type UpdateStashAttr struct {
	Column string      `json:"column" redis:"column"` //字段名
	Value  interface{} `json:"value" redis:"value"`   //值
	Incr   bool        `json:"incr" redis:"incr"`     //true：适用于可以累加的字段类型，false：代表替换
}

func NewUpdateStashAttr(column string, value interface{}, incr bool) UpdateStashAttr {
	return UpdateStashAttr{
		Column: column,
		Value:  value,
		Incr:   incr,
	}
}

// CreateHashStash 创建hash缓存对象
// key: 缓存的key
// m: 缓存的结构体
//
// 注意:hash的key优先使用结构体属性的`redis`标签值，为空则取字段名
func CreateHashStash(
	cli redis.UniversalClient,
	ctx context.Context,
	key string,
	m interface{},
) (err error) {
	var values []interface{}
	t := reflect.TypeOf(m)  //类型
	v := reflect.ValueOf(m) //值
	if t.Kind() == reflect.Pointer {
		t = t.Elem() //返回元素类型
		v = v.Elem()
	}

	//t.NumField 返回结构体的字段数量
	for k := 0; k < t.NumField(); k++ {
		col := t.Field(k).Tag.Get("redis")
		if col == "" {
			col = t.Field(k).Name //得到字段名，如name，age等
		}

		var val interface{}
		vField := v.Field(k)
		switch vField.Kind() {
		case reflect.String:
			val = vField.String()
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			val = vField.Int()
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			val = vField.Uint()
		case reflect.Float32, reflect.Float64:
			val = vField.Float()
		case reflect.Complex64, reflect.Complex128:
			val = vField.Complex()
		case reflect.Bool:
			val = vField.Bool()
		default:
			err = errors.New("unsupported type")
			return
		}
		//字段名，字段对应的值
		values = append(values, col, val)
	}
	// 把key / value添加到hash缓存中
	err = cli.HMSet(ctx, key, values...).Err()
	if err != nil {
		return
	}
	return
}

// UpdateHashStash 更新缓存hash数据-不存在则添加
// hashKey 缓存的哈希key
// changeListKey 存储发生变动hash key,不传则不存储
// notExistsHandler 当hashKey不存在时，触发的处理函数，返回的结构体将构建缓存，随后进行更新操作
func UpdateHashStash(
	cli redis.UniversalClient,
	ctx context.Context,
	hashKey string,
	changeListKey string,
	notExistsHandler RecordNotExistsHandler, attrs ...UpdateStashAttr,
) (err error) {
	if attrs == nil || len(attrs) == 0 {
		return
	}
	defer func() {
		if err == nil && changeListKey != "" {
			// SAdd往集合中添加元素，已经存在的元素被忽略
			err = cli.SAdd(ctx, changeListKey, hashKey).Err()
			if err != nil {
				return
			}
		}
	}()

	//先判断缓存中是否存在缓存hashKey对象，不存在则执行此匿名函数，然后将得到的值写入缓存
	if notExistsHandler != nil {
		var keyCount int64
		keyCount, err = cli.Exists(ctx, hashKey).Result()
		if err != nil {
			return
		}
		if keyCount == 0 {
			//采用分布式redis锁，只执行一次
			var l *LockAttr
			rl := NewRedisLock(cli)
			l, err = rl.RetryLock("update_hash_stash:"+hashKey, time.Minute*3, time.Minute)
			if err != nil {
				return
			}
			defer rl.Release(l.Key, l.Val) //释放锁

			var m interface{}
			m, err = notExistsHandler()
			if err != nil {
				return
			}
			err = CreateHashStash(cli, ctx, hashKey, m)
			if err != nil {
				return
			}
		}
	}

	//根据传入的属性，判断是递增还是覆盖
	pipe := cli.Pipeline()
	var values []interface{}
	for _, v := range attrs {
		if !v.Incr {
			values = append(values, v.Column, v.Value)
		} else {
			switch v.Value.(type) {
			case int64, int, int32, int8, int16:
				var incrVal int64
				switch v.Value.(type) {
				case int:
					incrVal = int64(v.Value.(int))
				case int8:
					incrVal = int64(v.Value.(int8))
				case int16:
					incrVal = int64(v.Value.(int16))
				case int32:
					incrVal = int64(v.Value.(int32))
				case int64:
					incrVal = v.Value.(int64)
				}
				//用于添加整数
				pipe.HIncrBy(ctx, hashKey, v.Column, incrVal)
			case float64:
				//类型float字段加上指定增量值
				pipe.HIncrByFloat(ctx, hashKey, v.Column, v.Value.(float64))
			default:
				err = errors.New("unsupported incr type")
			}
			if err != nil {
				log.Errorw("UpdaetHashStash error", err.Error(), "attr", str.ToJSON(v))
				continue
			}
		}
	}

	if values != nil && len(values) > 0 {
		pipe.HMSet(ctx, hashKey, values...)
	}
	_, err = pipe.Exec(ctx)
	if err != nil {
		return
	}
	return
}

// GetStash 获取当个hash对象值
func GetStash(cli redis.UniversalClient, ctx context.Context, key string, out interface{}) (err error) {
	if reflect.TypeOf(out).Kind() != reflect.Pointer {
		err = errors.New("out should be pointer")
		return
	}
	return cli.HGetAll(ctx, key).Scan(out)
}

//不建议redis做分页消费变动，存在丢失数据问题
