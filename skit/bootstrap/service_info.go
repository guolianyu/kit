package bootstrap

import "os"

type ServiceInfo struct {
	Name     string
	Version  string
	Id       string
	Metadata map[string]string //元数据
}

func NewServiceInfo(name, version, id string) ServiceInfo {
	if id == "" {
		id, _ = os.Hostname()
	}
	return ServiceInfo{
		Name:     name,
		Version:  version,
		Id:       id,
		Metadata: map[string]string{},
	}
}

// GetInstanceId 得到实例id名称，唯一性
func (s *ServiceInfo) GetInstanceId() string {
	return s.Id + "." + s.Name
}

// SetMetaData 设置服务元数据
func (s *ServiceInfo) SetMetaData(k, v string) {
	s.Metadata[k] = v
}
