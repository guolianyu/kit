package bootstrap

import (
	"errors"
	"gitee.com/guolianyu/kit/skit/third_party/config"
	translator "gitee.com/guolianyu/kit/skit/validate"
	"github.com/go-kratos/kratos/v2/log"
)

var (
// //go:embed ../../assets/lang/*.toml
// fs embed.FS
)

/*
	调用翻译器
*/

func NewValidateTranslator(conf *config.Language) (*translator.Translator, error) {
	if conf == nil {
		log.Errorf("[validate] config.Language is nil")
		return nil, errors.New("[validate] config.Language is nil")
	}
	locales := translator.NewLocales(conf.Trans...)
	t := translator.NewTranslator(locales...)
	path := conf.GetValidatePath()
	if path == "" {
		path = conf.Path
	}
	err := t.LoadLangFromPath(conf.Path)
	if err != nil {
		return nil, err
	}
	return t, nil
}
