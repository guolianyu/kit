package bootstrap

import (
	"errors"
	"gitee.com/guolianyu/kit/skit/middleware/localize"
	"gitee.com/guolianyu/kit/skit/third_party/config"
	"github.com/go-kratos/kratos/v2/log"
)

var (
// //go:embed ../../assets/lang/*.toml
// fs embed.FS
)

// 调用语言翻译包

func NewI18nBundle(conf *config.Language) (*localize.I18nBundle, error) {
	if conf == nil {
		log.Errorf("[i18n] config.Language is nil")
		return nil, errors.New("[i18n] config.Language is nil")
	}
	return localize.NewBundleFromPath(conf.Path)
}

func NewBundleFromFile(conf *config.Language) (*localize.I18nBundle, error) {
	if conf == nil {
		log.Errorf("[i18n] config.Language is nil")
		return nil, errors.New("[i18n] config.Language is nil")
	}
	return nil, nil
}
