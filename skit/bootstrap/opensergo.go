package bootstrap

import (
	"context"
	"gitee.com/guolianyu/kit/skit/third_party/config"
	"github.com/go-kratos/kratos/contrib/opensergo/v2"
	"github.com/go-kratos/kratos/v2"
)

func NewOpenSergo(app *kratos.App, cfg *config.OpenSergo) error {
	if cfg != nil && len(cfg.Endpoint) > 0 {
		sergo, err := opensergo.New(opensergo.WithEndpoint(cfg.GetEndpoint()))
		if err != nil {
			return err
		}
		if err = sergo.ReportMetadata(context.Background(), app); err != nil {
			return err
		}
	}
	return nil
}
