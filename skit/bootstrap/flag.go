package bootstrap

import "github.com/spf13/pflag"

/*
	命令标识
*/

var defaultBootstrapFlags = &ConfigFlags{
	ConfigPath: "../../configs",
	Env:        "dev",
}

// RegisterFlags 配合命令行选项使用 cobra
func RegisterFlags(flags *pflag.FlagSet) {

	//本地文件配置
	flags.StringVarP(&defaultBootstrapFlags.ConfigPath, "config", "c", "../../configs", "config path,eg: --conf bootstrap.yaml")

	flags.StringVarP(&defaultBootstrapFlags.Env, "env", "e", "dev", "runtime environment, eg: -env dev")

	// 远程服务配置数据，非本地文件加载
	flags.StringVarP(&defaultBootstrapFlags.ConfigType, "type", "t", "", "config provider, eg: --type consul")

	flags.StringSliceVarP(&defaultBootstrapFlags.ConfigHost, "servers", "s", []string{}, "config server host, eg: --serers 127.0.0.1:8500")
	flags.StringVarP(&defaultBootstrapFlags.UserName, "username", "u", "", "config server's username")
	flags.StringVarP(&defaultBootstrapFlags.Password, "password", "p", "", "config server's password")
}

func GetFlags() *ConfigFlags {
	return defaultBootstrapFlags
}
