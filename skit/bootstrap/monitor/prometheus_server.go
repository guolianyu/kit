package monitor

import (
	"fmt"
	"gitee.com/guolianyu/kit/skit/third_party/config"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	"github.com/go-kratos/kratos/v2/transport/http"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func NewPrometheusServer(c *config.Prometheus) *http.Server {
	if c == nil {
		return nil
	}
	if c.Host == "" {
		c.Host = ""
	}
	if c.Port <= 0 {
		c.Port = 9101
	}
	if c.Path == "" {
		c.Path = "/metrics"
	}

	var opts = []http.ServerOption{
		http.Address(fmt.Sprintf("%s:%d", c.Host, c.Port)),
		http.Middleware(
			recovery.Recovery(),
		),
	}

	srv := http.NewServer(opts...)
	srv.Handle(c.Path, promhttp.Handler())
	return srv

}
