package bootstrap

import (
	"gitee.com/guolianyu/kit/skit/third_party/config"
	"github.com/getsentry/sentry-go"
)

func NewSentry(cfg *config.Sentry) error {
	if cfg != nil {
		return sentry.Init(sentry.ClientOptions{
			Dsn:              cfg.GetDsn(),
			AttachStacktrace: cfg.GetAttachStackTrace(),
			Release:          cfg.GetRelease(),
			Environment:      cfg.GetEnvironment(),
			ServerName:       cfg.GetServerName(),
		})
	}
	return nil
}
