package bootstrap

import (
	"gitee.com/guolianyu/kit/skit/log/zapx"
	"gitee.com/guolianyu/kit/skit/third_party/config"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware/tracing"
	"go.uber.org/zap/zapcore"
	"os"
)

func NewLoggerProvider(cfg *config.Log, serviceInfo *ServiceInfo) log.Logger {
	var logger log.Logger
	if cfg != nil {
		if cfg.GetZap() != nil {
			l, err := zapcore.ParseLevel(cfg.GetLevel())
			if err != nil {
				l = zapcore.InfoLevel
			}
			logger = zapx.NewZapLoggerWithConfig(cfg.GetZap(), l)
		} else {
			logger = log.NewStdLogger(os.Stdout)
		}
		//todo 这边可以扩展其他日志框架

	} else {
		logger = log.NewStdLogger(os.Stdout)
	}
	log.SetLogger(logger)

	//格式
	logger = log.With(
		logger,
		"service.id", serviceInfo.Id,
		"service.name", serviceInfo.Name,
		"service.version", serviceInfo.Version,
		"ts", log.DefaultTimestamp,
		"caller", log.DefaultCaller,
		"trace_id", tracing.TraceID(),
		"span_id", tracing.SpanID(),
	)
	return logger
}
