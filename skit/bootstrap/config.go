package bootstrap

import (
	"fmt"
	"gitee.com/guolianyu/kit/filex"
	"gitee.com/guolianyu/kit/str"
	consulKratos "github.com/go-kratos/kratos/contrib/config/consul/v2"
	etcdKratos "github.com/go-kratos/kratos/contrib/config/etcd/v2"
	nacosKratos "github.com/go-kratos/kratos/contrib/config/nacos/v2"
	"github.com/go-kratos/kratos/v2/config"
	"github.com/go-kratos/kratos/v2/config/env"
	"github.com/go-kratos/kratos/v2/config/file"
	"github.com/hashicorp/consul/api"
	nacosClients "github.com/nacos-group/nacos-sdk-go/clients"
	nacosConstant "github.com/nacos-group/nacos-sdk-go/common/constant"
	"github.com/nacos-group/nacos-sdk-go/vo"
	etcdV3 "go.etcd.io/etcd/client/v3"
	GRPC "google.golang.org/grpc"
	"log"
	"net/url"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type ConfigFlags struct {
	ConfigPath  string   //文件路径
	ConfigType  string   //文件类型
	ConfigHost  []string //配置主机
	ServiceName string   //服务名称
	Env         string   //环境
	UserName    string   //用户名称
	Password    string   //密码
}

// NewConfigProvider kratos中使用config.Config 来加载文件和scan数据
func NewConfigProvider(cfg ConfigFlags) config.Config {

	options := []config.Source{
		env.NewSource(),
	}

	//获取从命令或默认的yaml文件路径
	if path, err := filepath.Abs(cfg.ConfigPath); err == nil {
		if filex.IsExists(path) {
			options = append(options,
				NewFileConfigSource(path),
			)
		}
	}

	//文件配置类型-主要适用于 远程服务配置，非本地文件配置
	configType := strings.ToLower(strings.TrimSpace(cfg.ConfigType))
	if len(configType) != 0 {
		if isSupportConfigProvider(configType) {
			options = append(options, NewRemoteConfigSource(&cfg))
		} else {
			log.Panic(fmt.Errorf("not support remote config provider %s", configType))
		}
	}

	return config.New(config.WithSource(options...))

	//c := config.New(
	//	config.WithSource(
	//		file.NewSource(flagconf),
	//	),
	//)
	//defer c.Close()
	//
	//if err := c.Load(); err != nil {
	//	panic(err)
	//}
	//
	//var bc conf.Bootstrap
	//if err := c.Scan(&bc); err != nil {
	//	panic(err)
	//}

}
func NewFileConfigSource(filePath string) config.Source {
	return file.NewSource(filePath)
}

const (
	ConfigEtcd   = "etcd"
	ConfigConsul = "consul"
	ConfigNacos  = "nacos"
)

// 判断传入的远程服务是否在对应的服务名称中
func isSupportConfigProvider(provider string) bool {
	return str.InArray(strings.ToLower(provider), []string{ConfigEtcd, ConfigConsul, ConfigNacos})
}

// NewRemoteConfigSource 远程服务配置选择
func NewRemoteConfigSource(cfg *ConfigFlags) config.Source {
	switch cfg.ConfigType {
	case ConfigNacos:
		return NewNacosConfigSource(cfg)
	case ConfigConsul:
		return NewConsulConfigSource(cfg)
	case ConfigEtcd:
		return NewEtcdConfigSource(cfg)
	}
	return nil
}

// NewNacosConfigSource 创建一个远程配置源 - Nacos
func NewNacosConfigSource(cfg *ConfigFlags) config.Source {
	var sc []nacosConstant.ServerConfig

	for _, configHost := range cfg.ConfigHost {
		uri, _ := url.Parse(configHost)
		h := strings.Split(uri.Host, ":")
		addr := h[0]
		port, _ := strconv.Atoi(h[1])

		sc = append(sc, *nacosConstant.NewServerConfig(addr, uint64(port)))
	}

	cc := nacosConstant.ClientConfig{
		TimeoutMs:            10 * 1000, // http请求超时时间，单位毫秒
		BeatInterval:         5 * 1000,  // 心跳间隔时间，单位毫秒
		UpdateThreadNum:      20,        // 更新服务的线程数
		LogLevel:             "debug",
		CacheDir:             "../../configs/cache", // 缓存目录
		LogDir:               "../../configs/log",   // 日志目录
		NotLoadCacheAtStart:  true,                  // 在启动时不读取本地缓存数据，true--不读取，false--读取
		UpdateCacheWhenEmpty: true,                  // 当服务列表为空时是否更新本地缓存，true--更新,false--不更新
		Username:             cfg.UserName,
		Password:             cfg.Password,
	}

	nacosClient, err := nacosClients.NewConfigClient(
		vo.NacosClientParam{
			ClientConfig:  &cc,
			ServerConfigs: sc,
		},
	)
	if err != nil {
		panic(err)
	}

	return nacosKratos.NewConfigSource(nacosClient,
		nacosKratos.WithGroup(getConfigKey(cfg.ServiceName, false)),
		nacosKratos.WithDataID("bootstrap.yaml"),
	)
}

// NewEtcdConfigSource 创建一个远程配置源 - Etcd
func NewEtcdConfigSource(cfg *ConfigFlags) config.Source {
	if len(cfg.ConfigHost) == 0 {
		panic("etcd hosts must be set.")
	}
	etcdClient, err := etcdV3.New(etcdV3.Config{
		Endpoints:   cfg.ConfigHost,
		Username:    cfg.UserName,
		Password:    cfg.Password,
		DialTimeout: time.Second, DialOptions: []GRPC.DialOption{GRPC.WithBlock()},
	})
	if err != nil {
		panic(err)
	}

	etcdSource, err := etcdKratos.New(etcdClient, etcdKratos.WithPath(getConfigKey(cfg.ServiceName, true)))
	if err != nil {
		panic(err)
	}

	return etcdSource
}

// NewConsulConfigSource 创建一个远程配置源 - Consul
func NewConsulConfigSource(cfg *ConfigFlags) config.Source {
	if len(cfg.ConfigHost) == 0 {
		panic("[CONFIG]  Consul hosts must be set.")
	}

	clientConfig := &api.Config{
		Address: cfg.ConfigHost[0],
	}
	if len(cfg.Password) > 0 && len(cfg.UserName) > 0 {
		clientConfig.HttpAuth = &api.HttpBasicAuth{
			Username: cfg.UserName,
			Password: cfg.Password,
		}
	}

	consulClient, err := api.NewClient(clientConfig)
	if err != nil {
		panic(fmt.Errorf("[CONFIG] init consul client fail，err:%v", err))
	}
	if len(cfg.ServiceName) == 0 {
		cfg.ServiceName = "/"
	}
	consulSource, err := consulKratos.New(consulClient,
		consulKratos.WithPath(getConfigKey(cfg.ServiceName, true)),
	)
	if err != nil {
		panic(fmt.Errorf("[CONFIG] init consul KV fail，err:%v", err))
	}

	return consulSource
}

// getConfigKey 获取合法的配置名
func getConfigKey(configKey string, useBackslash bool) string {
	if useBackslash {
		return strings.Replace(configKey, `.`, `/`, -1)
	} else {
		return configKey
	}
}
