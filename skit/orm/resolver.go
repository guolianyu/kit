package orm

import (
	conf "gitee.com/guolianyu/kit/skit/third_party/config"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// ResolverType resolver type
type ResolverType string

const (
	Source  ResolverType = "source"
	Replica ResolverType = "replica"
)

// Resolver provide multiple database support
type Resolver struct {
	Type ResolverType
}

// BuildDialector build gorm.Dialector
func BuildDialector(driver Driver, c *conf.Database_Resolver) (dialector gorm.Dialector, err error) {
	switch driver {
	case MySQL:
		dialector = mysql.New(mysql.Config{
			DriverName: driver.String(),
			DSN:        c.Source,
		})
	case PostgresSQL:
		dialector = postgres.New(postgres.Config{
			DriverName: driver.String(),
			DSN:        c.Source,
		})
	default:
		return nil, ErrUnsupportedType
	}

	return dialector, nil
}
