package errors

import (
	"context"
	"errors"
	"fmt"
	kerror "github.com/go-kratos/kratos/v2/errors"
	"testing"
)

func TestDefault(t *testing.T) {
	defer func() {
		if recover() != nil {
			t.Error("fail")
		}
	}()

	next := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, errors.New("panic reason")
	}
	_, e := Errors()(next)(context.Background(), "panic")
	t.Logf("succ and reason is %v", e)
}

func TestError(t *testing.T) {
	next := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, errors.New("panic reason")
	}

	_, e := Errors(WithHandler(func(ctx context.Context, req interface{}, err error) error {
		return kerror.InternalServer("RECOVERY", fmt.Sprintf("panic triggered: %v", err))
	}))(next)(context.Background(), "notPanic")
	if e != nil {
		t.Errorf(e.Error())
	}
}
