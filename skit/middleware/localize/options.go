package localize

func WithLanguage(lang string) Option {
	return func(o *options) {
		o.language = lang
	}
}

func WithBundle(bundle *I18nBundle) Option {
	return func(o *options) {
		o.bundle = bundle
	}
}
func WithHeaderKey(key string) Option {
	return func(o *options) {
		o.headerKey = key
	}
}
func WithMateKey(key string) Option {
	return func(o *options) {
		o.metaKey = key
	}
}
