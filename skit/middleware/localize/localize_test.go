package localize

import (
	"context"
	"github.com/go-kratos/kratos/v2/middleware"
	"reflect"
	"testing"
)

func TestFromContext(t *testing.T) {
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name string
		args args
		want *Localized
	}{
		// TODO: Add test cases.
		{
			name: "test",
			args: args{
				ctx: context.Background(),
			},
			want: &Localized{
				lang:     "en",
				bundle:   nil,
				localize: nil,
				funks:    nil,
			},
		},
		{
			name: "test",
			args: args{
				ctx: context.Background(),
			},
			want: &Localized{
				lang:     "en",
				bundle:   nil,
				localize: nil,
				funks:    nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FromContext(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FromContext() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestM(t *testing.T) {
	type args struct {
		ctx       context.Context
		messageId string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.

		{
			name: "",
			args: args{},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := M(tt.args.ctx, tt.args.messageId); got != tt.want {
				t.Errorf("M() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewContext(t *testing.T) {
	type args struct {
		ctx context.Context
		lan *Localized
	}
	tests := []struct {
		name string
		args args
		want context.Context
	}{
		// TODO: Add test cases.

		{
			name: "",
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewContext(tt.args.ctx, tt.args.lan); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewContext() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestServer(t *testing.T) {
	type args struct {
		opts []Option
	}
	tests := []struct {
		name string
		args args
		want middleware.Middleware
	}{
		// TODO: Add test cases.

		{
			name: "",
			args: args{},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Server(tt.args.opts...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Server() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestT(t *testing.T) {
	type args struct {
		ctx          context.Context
		messageId    string
		data         interface{}
		pluralCounts []interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "",
			args: args{},
			want: "",
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := T(tt.args.ctx, tt.args.messageId, tt.args.data, tt.args.pluralCounts...); got != tt.want {
				t.Errorf("T() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTransError(t *testing.T) {
	type args struct {
		ctx context.Context
		err error
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "",
			args:    args{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := TransError(tt.args.ctx, tt.args.err); (err != nil) != tt.wantErr {
				t.Errorf("TransError() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
