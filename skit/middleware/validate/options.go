package validate

import (
	"context"
	ut "github.com/go-playground/universal-translator"
	goValidator "github.com/go-playground/validator/v10"
)

// WithValidator 注入自定义的validator
func WithValidator(validate *goValidator.Validate) Option {
	return func(o *options) {
		o.validator = validate
	}
}

// WithUniversalTranslator 注入自定义的通用翻译器
func WithUniversalTranslator(uniTrans *ut.UniversalTranslator) Option {
	return func(o *options) {
		o.uniTrans = uniTrans
	}
}

// WithLocaleFn 注入自定义的语言获取函数
func WithLocaleFn(fn func(ctx context.Context) string) Option {
	return func(o *options) {
		o.localeFn = fn
	}
}

// WithHandlerFn 注入自定义的错误处理函数
func WithHandlerFn(fn HandlerFunc) Option {
	return func(o *options) {
		o.handler = fn
	}
}
