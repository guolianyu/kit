package validate

import (
	"context"
	translator "gitee.com/guolianyu/kit/skit/validate"
	"github.com/go-kratos/kratos/v2/metadata"
	"github.com/go-kratos/kratos/v2/transport"
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/pt_BR"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	govalidator "github.com/go-playground/validator/v10"
	"testing"
)

var _ transport.Transporter = (*Transport)(nil)

type Transport struct {
	kind      transport.Kind
	endpoint  string
	operation string
}

func (tr *Transport) Kind() transport.Kind {
	return tr.kind
}

func (tr *Transport) Endpoint() string {
	return tr.endpoint
}

func (tr *Transport) Operation() string {
	return tr.operation
}

func (tr *Transport) RequestHeader() transport.Header {
	return nil
}

func (tr *Transport) ReplyHeader() transport.Header {
	return nil
}

type LoginReq struct {
	Username string `validate:"required,checkTest"`
}

func TestValidator(t *testing.T) {
	next := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, nil
	}
	req := LoginReq{
		Username: "1",
	}
	translated := translator.NewTranslator(zh.New(), en.New(), pt_BR.New())

	err := translated.LoadLangFromPath("./lang")
	if err != nil {
		t.Error(err)
	}
	translated.RegisterValidation("checkTest", func(fl govalidator.FieldLevel) bool {
		if fl.Field().String() == "test" {
			return true
		}
		return false
	})

	translated.RegisterValidation("checkTest", func(fl govalidator.FieldLevel) bool {
		if fl.Field().String() == "test" {
			return true
		}
		return false
	})
	translated.RegisterTranslation("checkTest", func(ut ut.Translator) error {
		//return ut.Add("checkTest", fmt.Sprintf("{0} %v", ut.Locale()), true)
		return nil
	}, func(ut ut.Translator, fe govalidator.FieldError) string {
		t, _ := ut.T("checkTest", fe.Field())
		return t
	})

	// Handle with KindHTTP transport context
	ctx := transport.NewServerContext(context.Background(), &Transport{kind: transport.KindHTTP, endpoint: "endpoint", operation: "/package.service/method"})
	md := metadata.New()
	md.Set("x-md-global-language", "zh")
	ctx = metadata.NewServerContext(ctx, md)

	_, e := Validator(WithValidator(translated.Validator()), WithUniversalTranslator(translated.UniversalTranslator()))(next)(ctx, &req)
	if e != nil {
		t.Errorf(e.Error())
	}
}

func TestValidatorRecover(t *testing.T) {
	next := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, nil
	}
	req := LoginReq{
		Username: "1",
	}
	translated := translator.NewTranslator(zh.New(), en.New(), pt_BR.New())

	err := translated.LoadLangFromPath("./lang")
	if err != nil {
		t.Error(err)
	}
	translated.RegisterValidation("checkTest", func(fl govalidator.FieldLevel) bool {
		if fl.Field().String() == "test" {
			return true
		}
		return false
	})

	translated.RegisterValidation("checkTest", func(fl govalidator.FieldLevel) bool {
		if fl.Field().String() == "test" {
			return true
		}
		return false
	})
	translated.RegisterTranslation("checkTest", func(ut ut.Translator) error {
		//return ut.Add("checkTest", fmt.Sprintf("{0} %v", ut.Locale()), true)
		return nil
	}, func(ut ut.Translator, fe govalidator.FieldError) string {
		t, _ := ut.T("checkTest", fe.Field())
		return t
	})

	// Handle with KindHTTP transport context
	ctx := transport.NewServerContext(context.Background(), &Transport{kind: transport.KindHTTP, endpoint: "endpoint", operation: "/package.service/method"})
	md := metadata.New()
	md.Set("x-md-global-language", "en")
	ctx = metadata.NewServerContext(ctx, md)

	_, e := Validator(WithValidator(translated.Validator()), WithUniversalTranslator(translated.UniversalTranslator()))(next)(ctx, &req)
	if e != nil {
		t.Errorf(e.Error())
	}
}
