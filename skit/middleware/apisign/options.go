package httpsign

import (
	"context"
	"net/http"
)

type Option func(*options)

type options struct {
	signMatch MatchSignFuncMap
}

type SignFunc func(ctx context.Context, q *http.Request) error

type MatchSignFuncMap map[string]SignFunc

func WithMatchSignFuncMap(m MatchSignFuncMap) Option {
	return func(o *options) {
		o.signMatch = m
	}
}
